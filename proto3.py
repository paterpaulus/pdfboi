from enum import Enum, auto
from functools import partial
import wx
import wx.lib.sized_controls as sc
import wx.lib.mixins.inspection as WIT
from wx.lib.pdfviewer import pdfViewer

class State(Enum):
    INIT = auto()
    CLICKED = auto()
    DONE = auto()

SCROLLBAR_WIDTH=10
WINDOW_WIDTH=800
WINDOW_HEIGHT=600
A4WIDTH=210 # mm
A4HEIGHT=297 # mm
RATIO=(WINDOW_WIDTH - SCROLLBAR_WIDTH) / A4WIDTH
PAGEWIDTH=WINDOW_WIDTH # px
PAGEHEIGHT=RATIO * A4HEIGHT # px
SPACING=25 # px
FILE_NAME="exam.pdf"
LATEX_TEMPLATE="""
\\noindent\\makebox[\\textwidth]{{
  \\includegraphics[
    width=\paperwidth,
    clip,
    trim=0 {bottom}mm 0 {top}mm,
    page={page}
  ]{{{filename}}}
}}
"""

print(LATEX_TEMPLATE)

def get_page_position(absolute_y):
    page_h = PAGEHEIGHT + SPACING
    return int(absolute_y / page_h) + 1, (absolute_y % page_h) / RATIO


class PDFBoi(sc.SizedFrame):
    def __init__(self, parent, **kwds):
        super(PDFBoi, self).__init__(parent, **kwds)
        contents = self.GetContentsPane()

        self.viewer = pdfViewer(
            contents,
            wx.ID_ANY,
            wx.DefaultPosition,
            wx.DefaultSize,
            wx.HSCROLL | wx.VSCROLL | wx.SUNKEN_BORDER
        )
        self.viewer.SetSizerProps(expand=True, proportion=1)

        self.viewer.Bind(wx.EVT_LEFT_UP, self.OnClick)
        self.viewer.Bind(wx.EVT_RIGHT_UP, self.OnRightClick)

        # dirty fucking hack that I had to use
        # because binding to self.viewer/EVT_SCROLL
        # did not work for some reason.
        # Basically I just want to attach self#update_panels
        # to the viewers scroll event, but I'm very new to this.
        # Dirty hacks ftw.
        vr = self.viewer.Render
        self.viewer.Render = partial(lambda s : (
            self.update_panels(),
            vr()
        ), self.viewer)

        # initial state of the position collection FSM
        self.state = State.INIT
        # this will hold all the important regions
        self.regions = []
        self.y0 = None
        self.y1 = None
        self.init_panels()

    def OnScroll(self, event):
        print("Scrolled")
        self.update_panels()

    def OnClick(self, event):
        windowX, windowY = event.GetPosition()
        scrollY = self.get_viewer_scroll_pos()
        absoluteY = scrollY + windowY
        page, offset = get_page_position(absoluteY)
        print("click event received")
        print("click position: {}, {}".format(
            windowX,
            windowY
        ))
        print("scroll y: {}".format(
            scrollY
        ))
        print("absolute y: {}".format(
            absoluteY
        ))
        print("page: {} offset: {}mm".format(page, offset))

        if self.state is State.INIT:
            self.y0 = absoluteY
            self.create_upper_panel()
            self.state = State.CLICKED
        elif self.state is State.CLICKED:
            self.y1 = absoluteY
            self.create_lower_panel()
            print((self.y0, self.y1))
            self.state = State.DONE
        elif self.state is State.DONE:
            self.regions.append((self.y0, self.y1))
            self.destroy_panels()
            self.state = State.INIT

        self.update_panels()

    def OnRightClick(self, event):
        self.print_latex()


    def get_viewer_scroll_pos(self):
        unitX, unitY = self.viewer.GetScrollPixelsPerUnit()
        scrollYRaw = self.viewer.GetScrollPos(wx.VERTICAL)

        return scrollYRaw * unitY
    
    def init_panels(self):
        self.lower_panel = wx.Panel(
            self, pos=(0, 0), size=(WINDOW_WIDTH, 2)
        )
        self.lower_panel.SetBackgroundColour("red")
        self.lower_panel.Hide()

        self.upper_panel = wx.Panel(
            self, pos=(0, 0), size=(WINDOW_WIDTH, 2)
        )
        self.upper_panel.SetBackgroundColour("red")
        self.upper_panel.Hide()

        self.update_panels()

    def create_lower_panel(self):
        self.lower_panel.Show()

    def create_upper_panel(self):
        self.upper_panel.Show()

    def destroy_panels(self):
        self.upper_panel.Hide()
        self.lower_panel.Hide()

    def update_panels(self):
        print("updating panels")
        scrollY = self.get_viewer_scroll_pos()
        if self.y0 is not None:
            self.upper_panel.Move(0, self.y0 - scrollY)
        if self.y1 is not None:
            self.lower_panel.Move(0, self.y1 - scrollY)

    def print_latex(self):
        for region in self.regions:
            page0, offset_in_page0 = get_page_position(region[0])
            page1, offset_in_page1 = get_page_position(region[1])

            top_trim = offset_in_page0
            bottom_trim = A4HEIGHT - offset_in_page1

            print(LATEX_TEMPLATE.format(
                bottom=bottom_trim,
                top=top_trim,
                page=page0,
                filename=FILE_NAME
            ))

if __name__ == "__main__":
    app = WIT.InspectableApp(redirect=False)

    pb = PDFBoi(None, size=(WINDOW_WIDTH, WINDOW_HEIGHT))
    pb.viewer.LoadFile("exam.pdf")
    pb.Show()
    app.MainLoop()
