import os.path

A4_WIDTH=210 # mm
A4_HEIGHT=297 # mm
# RATIO=(WINDOW_WIDTH - SCROLLBAR_WIDTH) / A4WIDTH

class PosTranslator():
    def __init__(self, page_width, page_height, screen_width, page_spacing):
        self.page_width = page_width
        self.page_height = page_height
        self.virtual_width = screen_width
        self.page_spacing = page_spacing

        self.virtual_height = self.virtual_width * (self.page_height / self.page_width)

    def px_to_mm(self, px):
        return px * (self.page_width / self.virtual_width)

    def get_page_height(self):
        return self.page_height

    def get_physical(self, virtual):
        virtual_full = self.virtual_height + self.page_spacing
        page = int(virtual / virtual_full) + 1
        offset_in_page = self.px_to_mm(virtual % virtual_full)
        
        return page, offset_in_page

def write_latex(input_file_name, width, page_spacing, regions):
    with open("templates/file.tex", "r") as f:
        file_template = f.read()
    with open("templates/region.tex", "r") as f:
        region_template = f.read()
    pos_translator = PosTranslator(A4_WIDTH, A4_HEIGHT, width, page_spacing)
    regions_str = ""

    for (start, end) in regions:
        start_page, start_offset_in_page = pos_translator.get_physical(start)
        end_page, end_offset_in_page = pos_translator.get_physical(end)

        trim_top = start_offset_in_page
        trim_bottom = pos_translator.get_page_height() - end_offset_in_page

        regions_str = regions_str + region_template.format(
            trim_top=trim_top,
            trim_bottom=trim_bottom,
            page=start_page,
            filename=os.path.basename(input_file_name)
        )

    output_file_name = get_output_file_name(input_file_name)
    with open(output_file_name, "w") as output_file:
        output_file.write(file_template.format(regions=regions_str))

    print("{} regions written to {}".format(len(regions), output_file_name))

def get_output_file_name(input_file_name):
    pref, suff = os.path.splitext(input_file_name)

    return pref + "-cropped.tex"
