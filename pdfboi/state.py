from . import latex

class State:
    def __init__(self, app):
        self.app = app
    
    def left_click(self, click_x, click_y):
        pass

    def right_click(self, click_x, click_y):
        pass

    def keyup_l(self):
        print("l")
        self.app.write_latex()

class Init(State):
    def __init__(self, app):
        super().__init__(app)

    def left_click(self, click_x, click_y):
        absolute_y = self.app.get_scroll_y() + click_y
        self.app.y0 = absolute_y

        self.app.set_state(Clicked)

class Clicked(State):
    def __init__(self, app):
        super().__init__(app)

    def left_click(self, click_x, click_y):
        absolute_y = self.app.get_scroll_y() + click_y
        self.app.y1 = absolute_y

        self.app.set_state(Done)

    def right_click(self, click_x, click_y):
        self.app.y0 = None

        self.app.set_state(Init)

class Done(State):
    def __init__(self, app):
        super().__init__(app)

    def left_click(self, click_x, click_y):
        self.app.add_region()

        self.app.set_state(Init)

    def right_click(self, click_x, click_y):
        self.app.y1 = None

        self.app.set_state(Clicked)
