import functools
from . import state
from . import latex
import wx
import wx.lib.sized_controls as sc
from wx.lib.pdfviewer import pdfViewer

SCROLLBAR_WIDTH=10
PAGE_SPACING=24 # px

class App(sc.SizedFrame):
    def __init__(self, file_name, size):
        super().__init__(None, size=size)

        self.file_name = file_name
        self.width = size[0]
        self.height = size[1]

        contents = self.GetContentsPane()
        self.viewer = pdfViewer(
            contents,
            wx.ID_ANY,
            wx.DefaultPosition,
            wx.DefaultSize,
            wx.HSCROLL | wx.VSCROLL | wx.SUNKEN_BORDER
        )
        self.viewer.SetSizerProps(expand=True, proportion=1)

        self.viewer.Bind(wx.EVT_LEFT_UP, self.handle_left_click)
        self.viewer.Bind(wx.EVT_RIGHT_UP, self.handle_right_click)
        self.viewer.Bind(wx.EVT_KEY_UP, self.handle_key_up)

        # dirty fucking hack that I had to use
        # because binding to self.viewer/EVT_SCROLL
        # did not work for some reason.
        # Basically I just want to attach self#update_panels
        # to the viewers scroll event, but I'm very new to this.
        # Dirty hacks ftw.
        vr = self.viewer.Render
        self.viewer.Render = functools.partial(lambda s : (
            self.update_view(),
            vr()
        ), self.viewer)

        # initial state of the position collection FSM
        self.set_state(state.Init)

        self.y0 = None
        self.y1 = None
        self.regions = []

        self.init_view()
        self.viewer.LoadFile(self.file_name)

    def set_state(self, state_constructor):
        self.state = state_constructor(self)

    def handle_left_click(self, event):
        click_x, click_y = event.GetPosition()
        self.state.left_click(click_x, click_y)
        self.update_view()

    def handle_right_click(self, event):
        click_x, click_y = event.GetPosition()
        self.state.right_click(click_x, click_y)
        self.update_view()

    def handle_key_up(self, event):
        keycode = event.GetUnicodeKey()

        if keycode == ord("L"):
            self.state.keyup_l()
            self.update_view()

    def add_region(self):
        self.regions.append((self.y0, self.y1))
        self.y0 = None
        self.y1 = None

    def init_view(self):
        self.upper_selection_panel = self.create_panel()
        self.lower_selection_panel = self.create_panel()

    def create_panel(self):
        panel = wx.Panel(self, pos=(0, 0), size=(self.width, 2))
        panel.SetBackgroundColour("red")

        return panel

    def update_view(self):
        print("updating view {} {}".format(self.y0, self.y1))

        scroll_y = self.get_scroll_y()

        for (y, panel) in [
            (self.y0, self.upper_selection_panel),
            (self.y1, self.lower_selection_panel)
        ]:
            if y is None:
                panel.Hide()
            else:
                panel.Move(0, y - scroll_y)
                panel.Show()
    
    def get_scroll_y(self):
        unit_x, unit_y = self.viewer.GetScrollPixelsPerUnit()
        scroll_y = self.viewer.GetScrollPos(wx.VERTICAL)

        return unit_y * scroll_y

    def write_latex(self):
        latex.write_latex(
            width=self.width - SCROLLBAR_WIDTH,
            page_spacing=PAGE_SPACING,
            input_file_name=self.file_name,
            regions=self.regions
        )
