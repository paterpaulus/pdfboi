# pdfboi

Crops sections out of an A4 PDF and outputs latex commands to put them into a new document.

## Usage

See `doc/manual.md`.

## TO DO

- Remove dirty scroll listener hack
- Show existing regions, not just current selection.
