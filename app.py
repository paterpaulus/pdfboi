import pdfboi.gui
import os
import sys
import wx.lib.mixins.inspection as WIT

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: pdfboi <input file>")
        sys.exit(1)

    file_name = sys.argv[1]

    if not os.path.isfile(file_name):
        print("{} is not a file!".format(file_name))
        sys.exit(1)

    app = WIT.InspectableApp(redirect=False)

    pb = pdfboi.gui.App(file_name, size=(800, 600))
    pb.Show()

    app.MainLoop()
