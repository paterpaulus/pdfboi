# pdfboi manual

## Installation

`pdfboi` needs `wxPython` (a GUI library) and `PyMuPDF` (a PDF library) to work.
You can install those using `pip`:

```
$ pip install wxPython PyMuPDF
```

Then, start pdfboi by running `app.py` in the root of this repo:

```
$ cd path/to/pdfboi
$ python app.py path/to/exam.pdf
```

## Workflow

### Selecting a region

`pdfboi` lets you select regions from a PDF file and produces a LaTeX file that concatenates all those regions into a new PDF file.
Thusly, you can use `pdfboi` to cut regions from a file that are not important.

A region always fills the available horizontal space.
To select a region, left-click twice to set it's upper and lower boundaries.
Left-click again to confirm your selection.
Right-click at any point to un-set one of the boundaries.

### Generate LaTeX code

Press `l` to have `pdfboi` generate LaTeX code for the cropped output file.
If your input file is `<path>/<title>.pdf`, the output will be written to `<path>/<title>-cropped.tex`.

`pdfboi` uses `templates/region.tex` for generating the regions and `templates/file.tex` for the whole file.
Change them according to your needs.
