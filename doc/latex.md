# How I got to the horrible LaTeX code pdfboi produces

## First try

At first, I tired `\includepdf`, as it seemed to be the obvious choice:

```
\includepdf[trim, clip=0 193mm 0 71mm]{exam.pdf}
```

Turns out that put every single cropped region on a new page.
I guess you could circumvent this by using a `minipage`, but I've never used them.

## Second try

Then, `\includegraphics`.
The problems:

* You can't select the page (`\includegraphics` doesn't have no `pages` option).
* They are indented.

## Third try

```
% noindent cancels the indentation
% makebox[\textwidth], as I understand it, stops the nasty HBOX TOO WIDE!!11! messages
\noindent\makebox[\textwidth]{
    \includegraphics[
        % makes the image as wide as the page
    	width=\paperwidth,
	% crops the image to the correct size
	clip,
	% order: <left> <bottom> <right> <top>
	trim=0 193mm 0 71mm
    ]{exam.pdf}
}
```

The PDF file is viewed as a contiguous image, so the page offset is added in the trim option.

Actually, that didn't work, but after some googling and thinking I'd have to actually divide the file into files of its pages, I found out that `\includegraphics` has a `page` option:

## The solution

```
\noindent\makebox[\textwidth]{
    \includegraphics[
        % makes the image as wide as the page
    	width=\paperwidth,
	% crops the image to the correct size
	clip,
	% order: <left> <bottom> <right> <top>
	trim=0 193mm 0 71mm,
	page=1
    ]{exam.pdf}
}
```
